#!/usr/bin/env python

import os

import requests

from flask import Flask


def call(procedure, *args, **kwargs):
    url = '{}/caller-anonymous'.format(os.environ['AUTOMATE_HOSTNAME'])
    r = requests.post(
        url,
        json={'procedure': procedure, 'args': args, 'kwargs': kwargs}
    )

    print(r)


def publish(topic, *args, **kwargs):
    url = '{}/publish-anonymous'.format(os.environ['AUTOMATE_HOSTNAME'])
    r = requests.post(
        url,
        json={'topic': topic, 'args': args, 'kwargs': kwargs}
    )
    print(r)


app = Flask(__name__)


from . import pubsub  # noqa: F401


@app.route('/')
def hello():
    return 'hello.'
