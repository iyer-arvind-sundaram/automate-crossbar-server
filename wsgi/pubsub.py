#!/usr/bin/env python

import os

import requests
import xml.etree.ElementTree as ET
from flask import request

from . import app, publish, call


def discoverPuSHHub(url):
    r = requests.get(url)
    root = ET.fromstring(r.text)
    d = {r.attrib['rel']: r.attrib['href']
         for r in root if r.tag == '{http://www.w3.org/2005/Atom}link'}
    return d['hub']


def subscribePuSHHub(callback, url):
    hub = discoverPuSHHub(url)
    data = {'hub.mode': 'subscribe',
            'hub.topic': url,
            'hub.callback': callback}

    r = requests.post(hub, data=data)
    print(r.headers)


@app.route('/PuSH/subscribe')
def subscribe():
    callback = '{}/wsgi/PuSH/callback'.format(os.environ['AUTOMATE_HOSTNAME'])
    url = request.args.get('url')
    subscribePuSHHub(callback, url)
    return 'Subscribing to PuSH {} at {}'.format(url, callback)


@app.route('/PuSH/subscribe-youtube')
def subscribeYouTube():
    callback = '{}/wsgi/PuSH/callback'.format(os.environ['AUTOMATE_HOSTNAME'])

    url = r'https://www.youtube.com/xml/feeds/videos.xml?channel_id={}'
    subscribePuSHHub(callback, url.format(request.args.get('channel_id')))

    return 'Subscribing to PuSH {} at {}'.format(url, callback)


@app.route('/PuSH/callback', methods=['GET', 'POST'])
def callback():
    print('received callback from {}'.format(request.remote_addr))
    if request.method == 'GET':
        for i in request.args:
            print(i, request.args.get(i))

        topic = request.args.get('hub.topic')
        lease = int(request.args.get('hub.lease_seconds'))

        call('automate.pubsub.register_hub', topic=topic, lease=lease)

        return request.args.get('hub.challenge')

    else:
        ns = {'ty': 'http://www.youtube.com/xml/schemas/2015',
              'at': 'http://www.w3.org/2005/Atom'}

        e = ET.fromstring(request.data)
        if e.find('at:entry/at:author/at:name', ns) is None:
            print(request.data)
            return ''

        author = e.find('at:entry/at:author/at:name', ns).text
        title = e.find('at:entry/at:title', ns).text
        link = e.find('at:entry/at:link', ns).attrib['href']

        print('Publication received from {}: {}'.format(author, title))

        publish('automate.pubsub.publish',
                author=author, title=title, link=link)

    return ''
