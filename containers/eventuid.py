#!/usr/bin/env python

from collections import defaultdict
import datetime

import requests

from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker
from sqlalchemy import Table, Column, Integer, String, MetaData, ForeignKey, DateTime

Base = declarative_base()

from keys import db_str

from baseComponent import BaseSession, register, subscribe

class Event(Base):
    __tablename__ = 'Event'
    id = Column(Integer(), primary_key=True)
    uid = Column(String(32), unique=True)
    time = Column(DateTime())


class Events(BaseSession):
    def __init__(self, *args, **kwargs):
        self.engine = create_engine(db_str, echo=False)
        Event.metadata.create_all(self.engine)
        self.Session = sessionmaker(bind=self.engine)

        super().__init__(*args, **kwargs)

    @subscribe('automate.event.add', lvcache=False)
    def add(self, uid):
        session = self.Session()
        event = Event(uid=uid, time=datetime.datetime.now())
        session.add(event)
        session.commit()
        print('Registered event {}'.format(uid))

    @register('automate.event.check')
    def check(self, uid):
        session = self.Session()
        count = 0

        for i in session.query(Event).filter(Event.uid.like('%{}%'.format(uid))).all():
            count += 1

        return count


if __name__ == '__main__':
    from autobahn.twisted.wamp import ApplicationRunner
    runner = ApplicationRunner(url=u"ws://localhost:5000/ws", realm=u"realm1")
    #runner = ApplicationRunner(url=u"wss://arvindsiyer.herokuapp.com/ws", realm=u"realm1")
    runner.run(Events)
    
