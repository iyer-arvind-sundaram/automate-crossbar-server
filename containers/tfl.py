#!/usr/bin/env python

from collections import defaultdict
import datetime

import requests

from baseComponent import BaseSession, register

class TFLSession(BaseSession):
    def __init__(self, *args, **kwargs):
        self.app_key = '78fe0cffb9ef103a90870ea11264fe57'
        self.app_id = '497ee088'
        self.url = "https://api.tfl.gov.uk/StopPoint/{stop_id:s}/Arrivals"

        super().__init__(*args, **kwargs)

    @register('automate.tfl.arrivals')
    def getArrivalsInfo(self, stop_id):
        print("Calling")
        ret = defaultdict(list)
        r = requests.get(self.url.format(stop_id=stop_id))
        return r.json()
            

if __name__ == '__main__':
    from autobahn.twisted.wamp import ApplicationRunner
    runner = ApplicationRunner(url=u"ws://localhost:5000/ws", realm=u"realm1")
    #runner = ApplicationRunner(url=u"wss://arvindsiyer.herokuapp.com/ws", realm=u"realm1")
    runner.run(TFLSession)
    
