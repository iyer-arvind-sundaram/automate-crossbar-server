#!/usr/bin/env python

import re

from geopy.geocoders import Nominatim
from geopy.distance import vincenty

from baseComponent import BaseSession, register, subscribe
from persistentDict import PersistentDict

from sqlalchemy import Table, Column, Integer, String, Float
from sqlalchemy.ext.declarative import declarative_base
Base = declarative_base()


class NamedAreaDB(Base):
    __tablename__ = 'NamedArea'
    id = Column(Integer(), primary_key=True)
    name = Column(String(128), unique=True)
    latitude = Column(Float)
    longitude = Column(Float)
    radius = Column(Float)



class MobileArea(BaseSession):
    def __init__(self, *args, **kwargs):
        self.locator = Nominatim()
        self.db = PersistentDict(NamedAreaDB, 'name')
        super().__init__(*args, **kwargs)
    
    @subscribe('automate.mobile.location')
    def area(self, *args, **kwargs):
        latlon = kwargs['latitude'], kwargs['longitude']
        try:
            area = self.locator.reverse('{}, {}'.format(*latlon))
        except Exception as e:
            area = "~"
            pass
        
        named_areas = []
        for name, loc in self.db.items():
            platlon = loc['latitude'], loc['longitude']
            radius = loc['radius']
            distance = vincenty(latlon, platlon).meters
            if distance < radius:
                named_areas.append(name)
        else:
            named_area = ""

        self.publish('automate.mobile.area',
                     area=str(area), named_areas=named_areas)


    @register('automate.location.register')
    def namelocation(self, *args, **kwargs):
        lat, lon, name = (kwargs['latitude'], kwargs['longitude'],
                          kwargs['name'])

        radius = kwargs['radius'] if 'radius' in kwargs else 100
           
        
        name_an = re.sub('\W+', '_', name)
        self.db[name_an] = {'name': name, 'latitude': lat,
                            'longitude': lon, 'radius': radius}
        self.db.sync()
        return True


if __name__ == '__main__':
    from autobahn.twisted.wamp import ApplicationRunner
    import os
    if 'LOCAL_HOST' in os.environ:
        print('LOCAL HOST')
        runner = ApplicationRunner(url=u"ws://localhost:5000/ws", realm=u"realm1")
    else:
        print('HEROKU')
        runner = ApplicationRunner(url=u"wss://arvindsiyer.herokuapp.com/ws", realm=u"realm1")
    runner.run(MobileArea)
