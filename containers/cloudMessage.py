#!/usr/bin/env python

import json
import requests

from baseComponent import BaseSession, register, subscribe
from persistentDict import PersistentDict

class CloudMessage(BaseSession):
    def __init__(self, *args, **kwargs):
        self.url = 'https://llamalab.com/automate/cloud/message'
        self.secret = '1.FC6lBZ47eKlJEvYMFtWuU0PpRzhdIrnIDCM3Q7d0SHM='
        self.to = "iyer.arvind.sundaram@gmail.com"
        self.device = None

        super().__init__(*args, **kwargs)
    

    @register('automate.cloudmessage.display')
    def display(self, *args, **kwargs):
        payload = {'secret': self.secret,
                   'to': self.to,
                   'device': self.device,
                  }
        message = kwargs['message']
        payload['payload'] = json.dumps({'message': message, 'action': 'display'})
        print(payload)
        r = requests.post(self.url, data=payload)
        print(r)


if __name__ == '__main__':
    from autobahn.twisted.wamp import ApplicationRunner
    runner = ApplicationRunner(url=u"ws://localhost:5000/ws", realm=u"realm1")
    #runner = ApplicationRunner(url=u"wss://arvindsiyer.herokuapp.com/ws", realm=u"realm1")
    runner.run(CloudMessage)
