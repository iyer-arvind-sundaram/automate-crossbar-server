#!/usr/bin/env python

import datetime
import time
from pytz import timezone
import pytz

from twisted.logger import Logger
from autobahn.twisted.wamp import ApplicationSession
from autobahn.twisted.util import sleep
from twisted.internet.defer import inlineCallbacks
from autobahn.wamp import auth

from keys import wampcra_user, wampcra_key

class TimeSession(ApplicationSession):
    log = Logger()
    def __init__(self, config):
        self.log.info("TimeSession.__init__()")
        self.zones = [pytz.utc, pytz.timezone('Asia/Kolkata'),
                      pytz.timezone('Europe/London')]
        ApplicationSession.__init__(self, config)

    def onConnect(self):
        self.join(u'realm1', [u'wampcra'], wampcra_user)

    def onChallenge(self, challenge):
        if challenge.method == u'wampcra':
            signature = auth.compute_wcs(wampcra_key.encode('utf8'),
                                      challenge.extra['challenge'].encode('utf8'))
            return signature.decode('ascii')
        else:
            raise Exception('dont know how to handle authmethod {}'.format(challenge.method))

    @inlineCallbacks
    def onJoin(self, details):
        next_time = -1
        while True:
            now = datetime.datetime.now()
            for z in self.zones:
                now_z = now.astimezone(z)

                if now_z.hour < 6:
                    part = 'night'
                elif now_z.hour < 12:
                    part = 'morning'
                elif now_z.hour < 17:
                    part = 'afternoon'
                elif now_z.hour < 20:
                    part = 'evening'
                else:
                    part = 'night'

                timedct = {
                    'weekday_name': now_z.strftime('%a'),
                    'month_name': now_z.strftime('%b'),
                    'day_of_month': int(now_z.strftime('%d')),
                    'hour': now_z.hour,
                    'hour_float': now_z.hour + (now_z.minute+now_z.second/60)/60,
                    'part': part,
                    'day_of_year': now_z.timetuple().tm_yday,
                    'month': now_z.month,
                    'minute': now_z.minute,
                    'second': now_z.second,
                    'week': int(now_z.strftime('%W')),
                    'weekday': now_z.weekday(),
                    'year': now_z.year,
                    'weekend': now_z.strftime('%a') in ('Sat', 'Sun'),
                    'seconds_since_epoch': int(now_z.strftime('%s'))
                }
                self.publish('automate.time.{}'
                             .format(z).replace('/','-'),**timedct)
            
            yield sleep(max(next_time - time.time(), 0.1))

            next_time = int(time.time()+1)


if __name__ == '__main__':
    import os

    from autobahn.twisted.wamp import ApplicationRunner
    #runner = ApplicationRunner(url=u"ws://localhost:5000/ws", realm=u"realm1")
    runner = ApplicationRunner(url=u"{}/ws".format(os.environ['AUTOMATE_HOSTNAME'].replace('http','ws')), realm=u"realm1")

    runner.run(TimeSession)
