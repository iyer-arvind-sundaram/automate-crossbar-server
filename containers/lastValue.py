#!/usr/bin/env python

import datetime
import time

from autobahn.twisted.wamp import ApplicationSession
from autobahn.twisted.util import sleep
from autobahn.wamp import auth
from autobahn.wamp.types import SubscribeOptions                               
from twisted.internet.defer import inlineCallbacks
from twisted.logger import Logger

from sqlitedict import SqliteDict


class LastValueSession(ApplicationSession):
    log = Logger()
    def __init__(self, config):
        self.log.info("LastValueSession.__init__()")
        ApplicationSession.__init__(self, config)

    def onConnect(self):
        self.join(u'realm1', [u'wampcra'], u'_bot_')

    def onChallenge(self, challenge):
        if challenge.method == u'wampcra':
            signature = auth.compute_wcs(u'WzoqBhitsJWcNyN4Q42G1YETKSWjkCqbVaQlKsxN'.encode('utf8'),
                                      challenge.extra['challenge'].encode('utf8'))
            return signature.decode('ascii')
        else:
            raise Exception('dont know how to handle authmethod {}'.format(challenge.method))

    def saveValue(self, *args, **kwargs):
        details = kwargs.pop('details')
        with SqliteDict('/tmp/lv_cache.sqlite') as store:
            store[details.topic] = (args, kwargs)
            store.commit()

    def lastValue(self, topic):
        with SqliteDict('/tmp/lv_cache.sqlite') as store:
            if topic in store:
                return store[topic]
            else:
                return None

    def keys(self):
        with SqliteDict('/tmp/lv_cache.sqlite') as store:
            return list(store.keys())


    @inlineCallbacks
    def onJoin(self, details):
        yield self.subscribe(self.saveValue, 'automate',
                             SubscribeOptions(match='prefix',
                                              details_arg='details'))

        yield self.register(self.lastValue, 'automate.lastvalue')
        yield self.register(self.keys, 'automate.keys')

