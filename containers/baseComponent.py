#!/usr/bin/env python

from autobahn.twisted.wamp import ApplicationSession
from autobahn.wamp import auth
from autobahn.wamp.types import SubscribeOptions
from twisted.internet.defer import inlineCallbacks
from twisted.internet import reactor

from keys import wampcra_user, wampcra_key

class BaseSession(ApplicationSession):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.wampcra_user = wampcra_user
        self.wampcra_key = wampcra_key

    def onConnect(self):
        self.join(u'realm1', [u'wampcra'], self.wampcra_user)

    def onChallenge(self, challenge):
        if challenge.method == u'wampcra':
            signature = auth.compute_wcs(self.wampcra_key.encode('utf8'),
                                         challenge.extra['challenge'].encode('utf8'))
            return signature.decode('ascii')
        else:
            raise Exception('dont know how to handle authmethod {}'.format(challenge.method))

    @inlineCallbacks
    def onJoin(self, details):
        for i in dir(self):
            f = getattr(self, i)
            if hasattr(f, '__mode__'):
                if f.__mode__ == 'subscribe':
                    topics = f.__topics__
                    if f.__lvcache__:
                        for topic in topics:
                            print('lvcache for {} from {}'.format(f.__name__, topic))
                            ret = yield self.call('automate.lastvalue', topic)
                    
                        if ret is not None:
                            f(*ret[0], **ret[1])

                    for topic in topics:
                        print('subscribing {} to {}'.format(f.__name__, topic))
                        yield self.subscribe(f, topic, f.__options__)

                if f.__mode__ == 'register':
                    name = f.__rpc_name__
                    print('registering {} as {}'.format(f.__name__, name))
                    yield self.register(f, name, f.__options__)


def subscribe(topics, lvcache=True, options=None):
    if type(topics) is str:
        topics = [topics]

    def deco(func):
        func.__mode__ = 'subscribe'
        func.__topics__ = topics
        func.__options__ = options
        func.__lvcache__ = lvcache
        return func

    return deco


def register(name, options=None):
    def deco(func):
        func.__mode__ = 'register'
        func.__rpc_name__ = name
        func.__options__ = options
        return func
    return deco



def run(ComponentClass):
    from autobahn.twisted.wamp import ApplicationRunner
    #runner = ApplicationRunner(url=u'ws://arvindsiyer.herokuapp.com/ws',
    #                           realm=u'realm1')
    runner = ApplicationRunner(url=u'ws://localhost:5000/ws',
                               realm=u'realm1')
    # runner.set_protocol_options(autoPingTimeout=6000)
    runner.run(ComponentClass)

