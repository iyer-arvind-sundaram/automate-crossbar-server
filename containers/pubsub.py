#!/usr/bin/env python

import datetime
import os
import requests

from functools import partial

from twisted.internet import threads

from sqlalchemy import Table, Column, Integer, String, Float, DateTime
from sqlalchemy.ext.declarative import declarative_base

from baseComponent import BaseSession, register, subscribe
from persistentDict import PersistentDict

Base = declarative_base()
class PubSubDb(Base):
    __tablename__ = 'PubSub'
    id = Column(Integer(), primary_key=True)
    topic = Column(String(128), unique=True)
    date = Column(DateTime)
    lease = Column(Integer())


class PubSub(BaseSession):
    def __init__(self, *args, **kwargs):
        self.db = PersistentDict(PubSubDb, key='topic')
        self.sub_day = 0
        super().__init__(*args, **kwargs)

    @register('automate.pubsub.register_hub')
    def register_hub(self,topic, lease):
        print('Registering {}'.format(topic))
        self.db[topic] = {'date': datetime.datetime.now(), 'lease': lease}
        self.db.sync()
        return 'done'

    @subscribe('automate.time.UTC')
    def resubscribe(self, seconds_since_epoch, **_):
        if seconds_since_epoch > self.sub_day:
            self.sub_day = seconds_since_epoch + 86400
            for k in self.db.keys():
                print('Subscribing to {}'.format(k))
                self.subscribe_pubsub(k)
        


    @register('automate.pubsub.subscribe.youtube')
    def subscribe_youtube(self, channel_id):
        print("subscribing to {}".format(channel_id))
        print('{}/wsgi/PuSH/subscribe-youtube'.format(os.environ['AUTOMATE_HOSTNAME']))
        f = partial(
                requests.get,
                '{}/wsgi/PuSH/subscribe-youtube'.format(
                    os.environ['AUTOMATE_HOSTNAME']),
                params={'channel_id': channel_id}
        )
        d = threads.deferToThread(f)
        d.addCallback(lambda result:print(result))
        return 'requested'

    @register('automate.pubsub.subscribe')
    def subscribe_pubsub(self, url):
        print("subscribing to {}".format(url))
        print('{}/wsgi/PuSH/subscribe'.format(os.environ['AUTOMATE_HOSTNAME']))
        f = partial(
                requests.get,
                '{}/wsgi/PuSH/subscribe'.format(
                    os.environ['AUTOMATE_HOSTNAME']),
                params={'url': url}
        )
        d = threads.deferToThread(f)
        d.addCallback(lambda result:print(result))
        return 'requested'


if __name__ == '__main__':
    from autobahn.twisted.wamp import ApplicationRunner
    runner = ApplicationRunner(url=u"ws://localhost:5000/ws", realm=u"realm1")
    #runner = ApplicationRunner(url=u"wss://arvindsiyer.herokuapp.com/ws", realm=u"realm1")
    runner.run(PubSub)
