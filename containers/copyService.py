#!/usr/bin/env python

import datetime
import uuid

from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker
from sqlalchemy import String, Text, DateTime, Column, Integer, LargeBinary

Base = declarative_base()

from keys import db_str

from baseComponent import BaseSession, register
from persistentDict import PersistentDict

class CopyData(Base):
    __tablename__ = 'Copy'
    id = Column(Integer(), primary_key=True)
    key = Column(String(32), unique=True)
    data = Column(LargeBinary())
    time = Column(DateTime())


class Copy(BaseSession):
    def __init__(self, *args, **kwargs):
        self.db = PersistentDict(CopyData, 'key', approximate=True)

        super().__init__(*args, **kwargs)

    @register('automate.copypaste.copy')
    def copy(self, data):
        if type(data) is not bytes:
            raise TypeError('Data must be bytes')

        key = uuid.uuid4().hex
        self.db[key] = {'data': data, 'time':datetime.datetime.now()}
        self.db.sync()
        return key


    @register('automate.copypaste.paste')
    def paste(self, key):
        if key in self.db:
            return self.db[key]['data']

        return None


if __name__ == '__main__':
    from autobahn.twisted.wamp import ApplicationRunner
    #runner = ApplicationRunner(url=u"ws://localhost:5000/ws", realm=u"realm1")
    runner = ApplicationRunner(url=u"wss://arvindsiyer.herokuapp.com/ws", realm=u"realm1")
    runner.run(Copy)
    
