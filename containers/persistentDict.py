#!/usr/bin/env python

import datetime
import hmac
import json
import time
import random
import requests
import hashlib
import collections
from contextlib import contextmanager


from sqlalchemy import create_engine
from sqlalchemy.pool import QueuePool
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker
from sqlalchemy import Table, Column, Integer, String, MetaData, ForeignKey, DateTime, Boolean, Text

import keys

engine = create_engine(keys.db_str, echo=False, poolclass=QueuePool, pool_size=5)
Session = sessionmaker(bind=engine)

@contextmanager
def session_scope():
    """Provide a transactional scope around a series of operations."""
    session = Session()
    try:
        yield session
        session.commit()
    
    except:
        session.rollback()
        raise

    finally:
        session.close()

def retry(count):
    def decorator(func):
        def newfunc(*args, **kwargs):
            for trial in range(count):
                try:
                    return func(*args, **kwargs)
                except Exception as e:
                    print(e)
                    time.sleep(5)
                    print('retry db query')
                    pass

        return newfunc
    return decorator




class RemoteDict(collections.MutableMapping):
    def __init__(self, DBObjectClass, key, approximate=False, *args, **kwargs):
        self.DBObjectClass = DBObjectClass
        self.DBObjectClass.metadata.create_all(engine)
        self.key = key
        self.approximate = approximate

        super().__init__(*args, **kwargs)

    @retry(10)
    def __setitem__(self, key, value):
        if type(key) is not str:
            raise TypeError('Only string keys')

        value_keys = {self.key}
        for k in value:
            value_keys.add(k)
        table_keys = set(i.name for i in self.DBObjectClass.__table__.columns)

        if(len(value_keys - table_keys)):
            raise ValueError('Some new columns')

        values = {k:v for k,v in value.items()}
        values[self.key] = key

        with session_scope() as session:
            ret = session.query(self.DBObjectClass).filter_by(**{self.key: key}).first()
            if ret:
                print('Updating {}'.format(key))
                for k, v in values.items():
                    setattr(ret, k, v)

            else:
                print('New element: {}'.format(key))
                data = self.DBObjectClass(**values)
                session.add(data)

            session.commit()

    @retry(10)
    def __getitem__(self, key):
        if type(key) is not str:
            raise TypeError('Only string keys')

        with session_scope() as session:
            ret = session.query(self.DBObjectClass).filter_by(**{self.key: key}).first()
            
            if ret is None and self.approximate:
                print('Searching approximate match')
                col = getattr(self.DBObjectClass, self.key)
                ret = session.query(self.DBObjectClass).filter(col.like('%{}%'.format(key))).first()

            else:
                print('Found exact match')

            if ret is None:
                print('Key {} not found'.format(key))
                raise KeyError(key)

            table_keys = set(i.name for i in self.DBObjectClass.__table__.columns)
            dct = {}

            for k in table_keys:
                if k == self.key:
                    continue

                v = getattr(ret, k)
                dct[k] = v

            return dct

    @retry(10)
    def __delitem__(self, key):
        with session_scope() as session:
            ret = session.query(self.DBObjectClass).filter_by(**{self.key: key}).first()
            if ret:
                session.delete(ret)

    @retry(10)
    def __contains__(self, key):
        with session_scope() as session:
            ret = session.query(self.DBObjectClass).filter_by(**{self.key: key}).first()

            if ret is None and self.approximate:
                col = getattr(self.DBObjectClass, self.key)
                ret = session.query(self.DBObjectClass).filter(col.like('%{}%'.format(key))).first()

            return ret is not None

    @retry(10)
    def __len__(self):
        with session_scope() as session:
            return session.query(self.DBObjectClass).count()

    @retry(10)
    def __keys__(self):
        with session_scope() as session:
            keys = [i[0] for i in  session.query(getattr(self.DBObjectClass, self.key))]
            return keys

    @retry(10)
    def __iter__(self):
        with session_scope() as session:
            keys = [i[0] for i in  session.query(getattr(self.DBObjectClass, self.key))]

        for i in keys:
            yield i


class PersistentDict(collections.MutableMapping):
    def __init__(self, *args, **kwargs):
        self._cache = {}
        self._flush = {}
        self._dbm = RemoteDict(*args, **kwargs)

    def __contains__(self, key):
        return key in self._cache or key in self._dbm

    def __getitem__(self, key):
        if key in self._cache:
            return self._cache[key]
        return self._cache.setdefault(key, self._dbm[key])

    def __setitem__(self, key, value):
        self._cache[key] = self._flush[key] = value

    def __delitem__(self, key):
        found = False
        for data in (self._cache, self._flush, self._dbm):
            if key in data:
                del data[key]
                found = True
        if not found:
            raise KeyError(key)

    def __iter__(self):
        keys = set(self._cache.keys())
        keys.update(self._dbm.keys())
        for i in keys:
            yield i

    def dirty(key):
        self._flush[key] = self[key]

    def __len__(self):
        keys = set(self._cache.keys())
        keys.update(self._dbm.keys())
        return len(keys)

    def sync(self):
        for key, value in self._flush.items():
            self._dbm[key] = value
        self._flush = {}


def test():
    Base = declarative_base()
    class TestORDB(Base):
        __tablename__ = 'TestTable'
        id = Column(Integer(), primary_key=True)
        a = Column(String(32), unique=True)
        b = Column(Text)
        c = Column(Boolean)

        def repr(self):
            return '{} {} {} {}'.format(self.a, self.b, self.c, self.c)
    
    table = RemoteDict(TestORDB, 'a')
    table['a'] = {'b': 'this is a test', 'c': True}
    print(len(table))
    print(table['a'])
    table['b'] =  {'b': 'this also is a test', 'c': True}
    print(len(table))
    print(list(table.keys()))



if __name__ == '__main__':
    test()
