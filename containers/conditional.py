#!/usr/bin/env python

import io
import re
import sys
import time
import traceback
import types
from functools import partial

from twisted.logger import Logger
from autobahn.twisted.wamp import ApplicationSession
from autobahn.twisted.util import sleep
from twisted.internet.defer import inlineCallbacks
from autobahn.wamp import auth
from autobahn.wamp.types import SubscribeOptions

from baseComponent import BaseSession, register, subscribe
from persistentDict import PersistentDict
import keys

class DotDict(dict):
    __getattr__ = dict.__getitem__
    __setattr__ = dict.__setitem__


class Capturing(list):
    def __enter__(self):
        self._stdout = sys.stdout
        sys.stdout = self._stringio = io.StringIO()
        return self

    def __exit__(self, *args):
        self.extend(self._stringio.getvalue().splitlines())
        del self._stringio    # free up some memory
        sys.stdout = self._stdout


class Conditional(object):
    log = Logger()
    def __init__(self, session, source, name):
        self.session = session
        self.subscriptions = []
        self.last_call = None
        self.name = name
        self.update(source)

    
    @inlineCallbacks
    def messageBack(self, message):
        cb = 'automate.conditional.callback.{}'.format(self.name)
        func_avail = yield self.session.call('wamp.registration.lookup', cb)
    
        #print(message)
        #self.log.info(str(message))

        if func_avail:
            yield self.session.call(cb, self.name, message)

        yield


    def disable(self):
        for s in self.subscriptions:
            s.unsubscribe()

        self.subscriptions = []

    def delay(self, t):
        self.next_call = time.time() + t

    def update(self, source):
        try:
            res = self._update(source)
            msg =  {'status': 'Updated', 'result': True, 'print': []}

        except SyntaxError as e:
            msg = {'status': 'Error', 'error': str(e), 'result': None, 'print': []}

        except Exception as e:
            f = io.StringIO()
            traceback.print_exc(file=f)
            msg = {'status': 'Error', 'error': f.getvalue(), 'result': None, 'print': []}
        
        self.messageBack(msg)

    def _update(self, source):
        session = self.session
        self.disable()
        
        self.module = types.ModuleType(self.name)
        self.module.__dict__['subscribe'] = subscribe
        self.module.__dict__['session'] = self.session
        self.module.__dict__['delay'] = self.delay
    
        # print(source)
        exec(source, self.module.__dict__)
        
        self.dict = DotDict()

        self.avail_topics = set()
        self.func = func = self.module.main
        self.next_call = 0

        def lvCallback(top, ret):
            if ret is not None:
                self.log.info('LV available for {}'.format(top))
                details = type('details', (object,), {})()
                details.topic = top
                ret[1]['_details_'] = details
                self(*ret[0], **ret[1])

            else:
                self.log.info('No LV for {}'.format(top))
                pass

        def subscribeCallback(top, subscription):
            self.log.info('Subscribed to {}'.format(topic))
            self.subscriptions.append(subscription)

        if hasattr(func, '__topics__'):
            self.req_topics = set(func.__topics__)
            for topic in func.__topics__:
                session.call('automate.lastvalue', topic
                        ).addCallback(partial(lvCallback, topic))

                session.subscribe(self, topic,
                                  SubscribeOptions(details_arg='_details_')
                        ).addCallback(partial(subscribeCallback, topic))
        else:
            self.req_topics = []
            self(*(), **{})

    
    def __call__(self, *args, **kwargs):
        details = kwargs.pop('_details_')
        topic = details.topic
        self.avail_topics.add(topic)

        d = self.dict
        for t in topic.split('.')[1:]:
            if t not in d:
                d[t] = DotDict()

            d = d[t]

        for k, v in kwargs.items():
            d[k] = v
        
        if time.time() > self.next_call:
            self.messageBack(self.eval())

    def eval(self):
        fmt_set = lambda s: ", ".join([i for i in s])
        # self.log.info("Avail: " + fmt_set(self.avail_topics) + ", " +
        #              "Req: " + fmt_set(self.avail_topics) + ", " +
        #              "Not Avail: " + fmt_set(self.req_topics - self.avail_topics))

        if not (self.req_topics - self.avail_topics):
            try:
                with Capturing() as c:
                    res = self.func(**self.dict)

                return {'status': 'Executed', 'result': res, 'print': c}

            except Exception as e:
                f = io.StringIO()
                traceback.print_exc(file=f)

                self.disable()

                return {'status': 'Error',
                        'error': f.getvalue(),
                        'result': None}

        else:
            unav_topics = self.req_topics - self.avail_topics
            return {'status': 'Waiting',
                    'error': 'Waiting for {}'.format(fmt_set(unav_topics)),
                    'result': None
                   }


from sqlalchemy import Table, Column, Integer, String, MetaData, ForeignKey, DateTime, Boolean, Text
from sqlalchemy.ext.declarative import declarative_base
Base = declarative_base()


class ConditionalDB(Base):
    __tablename__ = 'Conditional'
    id = Column(Integer(), primary_key=True)
    name = Column(String(128), unique=True)
    comment = Column(Text)
    source = Column(Text)
    enabled = Column(Boolean)


class ConditionalSession(BaseSession):
    log = Logger()
    def __init__(self, *args, **kwargs):
        self.db = PersistentDict(ConditionalDB, 'name')
        self.conditionals = {}
        super().__init__(*args, **kwargs)
    
    @register('automate.conditional.names')
    def names(self):
        self.log.info('conditional names')
        return list(self.db.keys())

    @register('automate.conditional.load')
    def updateConditional(self, name):
        self.log.info('loading conditional {}'.format(name))
        if name in self.db:
            ret = self.db[name]
            if ret['enabled']:
                if name in self.conditionals:
                    self.log.info('Updating conditional {}'.format(name))
                    self.conditionals[name].update(ret['source'])
                else:
                    self.log.info('Adding conditional {}'.format(name))
                    self.conditionals[name] = Conditional(self, self.db[name]['source'], name)
            else:
                if name in self.conditionals:
                    self.log.info('Deleting conditional {}'.format(name))
                    self.conditionals[name].disable()
                    self.conditionals.pop(name)

            return True

        else:
            return False
    
    @register('automate.conditional.enable')
    def enableConditional(self, name, enable):
        self.log.info('setting conditional {} enabled to {}'.format(name, enable))
        if name not in self.db:
            raise ValueError("Wrong name")
        
        cnd = self.db[name]
        cnd['enabled'] = enable
        self.db[name] = cnd
        self.db.sync()

        self.updateConditional(name)


    @register('automate.conditional.getConditional')
    def getConditional(self, name):
        self.log.info('get conditional {}'.format(name))
        if name in self.db:
            ret = self.db[name]
            return ret
        return False
    
    @register('automate.conditional.save')
    def saveConditional(self, oldname, newname, comment, source, enabled):
        self.log.info('save conditional {} as {}'.format(oldname, newname))
        newname = newname.strip()

        if not len(newname):
            raise ValueError("Name cannot be missing")
        
        if oldname is not None:
            oldname = oldname.strip()
            if len(oldname) and oldname != newname:
                del self.db[oldname]

        self.db[newname] = {'comment':comment, 'enabled':enabled ,'source': source}
        self.db.sync()
        self.updateConditional(newname)
        self.db.sync()

    @register('automate.conditional.remove')
    def removeConditional(self, name):
        self.log.info('removing conditional {}'.format(name))
        if name in self.conditionals:
            del self.conditionals[name]

        if name is self.db:
            del self.db[name]
            self.db.sync()

    @inlineCallbacks
    def onJoin(self, details):
        yield from BaseSession.onJoin(self, details)

        for key in self.db:
            self.updateConditional(key)




if __name__ == '__main__':
    from autobahn.twisted.wamp import ApplicationRunner
    runner = ApplicationRunner(url=u"ws://localhost:5000/ws", realm=u"realm1")
    runner.run(ConditionalSession)

